namespace unionhandling

open WebSharper
open WebSharper.JavaScript
open WebSharper.UI
open WebSharper.UI.Client
open WebSharper.UI.Templating

module UnionHelper =
    open Microsoft.FSharp.Reflection

    let GetAllUnionCases<'T>() =
        FSharpType.GetUnionCases(typeof<'T>)
        |> Seq.map (fun x -> FSharpValue.MakeUnion(x, Array.zeroCreate(x.GetFields().Length)) :?> 'T)

module Union =
    type Union =
        | A
        | B

        static member AsList () = UnionHelper.GetAllUnionCases<Union>()

module ProxyMacro =
    open WebSharper.Core
    open WebSharper.Core.Macros
    open WebSharper.Core.AST
    type UnionAllMacro() =
        inherit WebSharper.Core.Macro()

        override this.TranslateCall(c) =
            let unionType = c.DefiningType
            let customTypeInfo = c.Compilation.GetCustomTypeInfo unionType.Entity
            match customTypeInfo with
            | Metadata.FSharpUnionInfo union ->
                union.Cases
                |> List.choose (fun x ->
                    if x.Kind <> Metadata.FSharpUnionCaseKind.SingletonFSharpUnionCase then
                        None
                    else
                        Some <| Expression.NewUnionCase(unionType, x.Name, [])
                )
                |> Expression.NewArray
                |> MacroResult.MacroOk
            | _ -> MacroError "This macro is only usable on a discriminated union type"

[<JavaScript>]
module internal Proxy =
    [<Proxy(typeof<Union.Union>)>]
    [<JavaScript>]
    type UnionProxy =
        | A
        | B

        [<Macro(typeof<ProxyMacro.UnionAllMacro>)>]
        static member AsList() = X<Union.Union seq>

[<JavaScript>]
module Client =
    // The templates are loaded from the DOM, so you just can edit index.html
    // and refresh your browser, no need to recompile unless you add or remove holes.
    type IndexTemplate = Template<"wwwroot/index.html", ClientLoad.FromDocument>

    let People =
        ListModel.FromSeq [
            "John"
            "Paul"
        ]


    [<SPAEntryPoint>]
    let Main () =
        let newName = Var.Create ""
        Console.Log (Union.Union.AsList())
        IndexTemplate.Main()
            .ListContainer(
                People.View.DocSeqCached(fun (name: string) ->
                    IndexTemplate.ListItem().Name(name).Doc()
                )
            )
            .Name(newName)
            .Add(fun _ ->
                People.Add(newName.Value)
                newName.Value <- ""
            )
            .Doc()
        |> Doc.RunById "main"
